﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApiAutores.Filtros
{
    public class FiltrosCustom : IActionFilter
    {
        private readonly ILogger<FiltrosCustom> logger;

        public FiltrosCustom(ILogger<FiltrosCustom> logger)
        {
            this.logger = logger;

        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            logger.LogInformation("After accion custom filter");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            logger.LogInformation("Before custom filter");
        }
    }
}
