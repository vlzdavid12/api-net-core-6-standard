﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace WebApiAutores.Filtros
{
    public class FiltroOfException : ExceptionFilterAttribute
    {
        private readonly ILogger<FiltroOfException> logger;

        public FiltroOfException(ILogger<FiltroOfException> logger)
        {
            this.logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            logger.LogError(context.Exception, context.Exception.Message);
            base.OnException(context);
        }
    }
}
