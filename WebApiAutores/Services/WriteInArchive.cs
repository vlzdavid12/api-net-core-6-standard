﻿namespace WebApiAutores.Services
{
    public class WriteInArchive : IHostedService
    {
        private readonly string nameArchive = "archive_1.txt";
        private readonly IHostEnvironment env;
        private Timer timer;

        public WriteInArchive(IHostEnvironment env)
        {
            this.env = env;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            timer =  new Timer(Dowork, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));
            WriterApp("Process Initial");
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            timer.Dispose();
            WriterApp("Process End");
            return Task.CompletedTask;
        }

        private void Dowork(Object state)
        {
            WriterApp("Proccess in execution: " +  DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
        }

        private void WriterApp(string message)
        {
            var ruta = $@"{env.ContentRootPath}\wwwroot\{nameArchive}";
            using (StreamWriter writer = new StreamWriter(ruta, append: true))
            {
                writer.WriteLine(message);
            }
        }
    }
}
