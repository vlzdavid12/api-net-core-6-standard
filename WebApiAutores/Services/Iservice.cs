﻿
namespace WebApiAutores.Services
{
    public interface IService
    {
        Guid GetTransient();
        Guid GetScoped();

        Guid GetSingleton();

        void WorkingPointer();
    }

    public class ServiceA : IService
    {
        private readonly ILogger<ServiceA> logger;
        private readonly ServiceTransient serviceTransient;
        private readonly ServiceSingleton serviceSingleton;
        private readonly ServiceScoped serviceScoped;

        public ServiceA(ILogger<ServiceA> logger,
            ServiceTransient serviceTransient,
            ServiceSingleton serviceSingleton,
            ServiceScoped serviceScoped)
        {
            this.logger = logger;
            this.serviceTransient = serviceTransient;
            this.serviceSingleton = serviceSingleton;
            this.serviceScoped = serviceScoped;
        }

        public void WorkingPointer()
        {

        }

        public Guid GetTransient() { return serviceTransient.Guid; }
        public Guid GetScoped() { return serviceScoped.Guid; }
        public Guid GetSingleton() { return serviceSingleton.Guid; }
    }



    public class ServiceB : IService
    {
        public Guid GetScoped()
        {
            throw new NotImplementedException();
        }

        public Guid GetSingleton()
        {
            throw new NotImplementedException();
        }

        public Guid GetTransient()
        {
            throw new NotImplementedException();
        }

        public void WorkingPointer()
        {

        }
    }

    public class ServiceTransient
    {

        public Guid Guid = Guid.NewGuid();
    }

    public class ServiceScoped
    {
        public Guid Guid = Guid.NewGuid();
    }

    public class ServiceSingleton
    {
        public Guid Guid = Guid.NewGuid();

    }
}
