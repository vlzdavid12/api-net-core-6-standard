﻿namespace WebApiAutores.Middlewares
{

    public static class LoguearRequestHTTPMiddlewareExtensions
    {
        public static IApplicationBuilder UseLoguearAnswerHTTP(this IApplicationBuilder app)
        {
            return app.UseMiddleware<LoguearRequestHTTPMiddleware>();
        }
    }


    public class LoguearRequestHTTPMiddleware
    {
        private RequestDelegate next;

        public LoguearRequestHTTPMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext contexto, ILogger<LoguearRequestHTTPMiddleware> logger)
        {

            using (var ms = new MemoryStream())
            {
                var bodyOriginalAnswer = contexto.Response.Body;
                contexto.Response.Body = ms;

                await next(contexto);

                ms.Seek(0, SeekOrigin.Begin);
                string answer = new StreamReader(ms).ReadToEnd();
                ms.Seek(0, SeekOrigin.Begin);

                await ms.CopyToAsync(bodyOriginalAnswer);
                contexto.Response.Body = bodyOriginalAnswer;

                logger.LogInformation(answer);
            }

        }
    }
}
