﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiAutores.Entidades;


namespace WebApiAutores.Controllers
{
    [ApiController]
    [Route("api/libros")]
    public class BooksController : ControllerBase
    {
        private readonly ApplicationDbContext context;

        public BooksController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Book>> Get(int id)
        {
            var existBook = await context.Books.AnyAsync(x => x.Id == id);

            if (!existBook)
            {
                return BadRequest($"This id not exist {id}");
            }

            return await context.Books.Include(x => x.Author).FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPost]
        public async Task<ActionResult> Post(Book book)
        {
            var existAuthor = await context.Authors.AnyAsync(x => x.Id == book.AuthorId);

            if (!existAuthor)
            {

                return BadRequest($"Not exist author of Id: {book.AuthorId}");
            }

            context.Add(book);
            await context.SaveChangesAsync();
            return Ok();
        }

    }
}
