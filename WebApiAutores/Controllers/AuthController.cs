﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApiAutores.Entidades;
using WebApiAutores.Filtros;
using WebApiAutores.Services;


namespace WebApiAutores.Controllers
{
    [ApiController]
    [Route("api/autores")]
    //[Authorize]
    public class AuthController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IService service;
        private readonly ServiceTransient serviceTransient;
        private readonly ServiceScoped serviceScoped;
        private readonly ServiceSingleton serviceSingleton;
        private readonly ILogger<AuthController> logger;

        public AuthController(
            ApplicationDbContext context,
            IService service,
            ServiceTransient serviceTransient,
            ServiceScoped serviceScoped,
            ServiceSingleton serviceSingleton,
            ILogger<AuthController> logger)
        {
            this.context = context;
            this.service = service;
            this.serviceTransient =  serviceTransient;
            this.serviceScoped =  serviceScoped;
            this.serviceSingleton =  serviceSingleton;
            this.logger = logger;
        }

        [HttpGet("GUID")]
        //[ResponseCache(Duration = 10)]
        [ServiceFilter(typeof(FiltrosCustom))]
        public ActionResult GetGuids()
        {
            return Ok(new
            {
                AuthorsController_Transient = serviceTransient.Guid,
                ServiceA_Transient = service.GetTransient(),
                AuthorsController_Scoped = serviceScoped.Guid,
                ServiceB_Scoped = service.GetScoped(),
                AuthorConTroller_Singleton = serviceSingleton.Guid,
                ServiceA_Singleton = service.GetSingleton(),
            });
        }

        [HttpGet] // api/autores
        [HttpGet("list")] // api/autores/list
        [HttpGet("/list")] // api/list
        //[ResponseCache(Duration = 10)]
        [ServiceFilter(typeof(FiltrosCustom))]
        public async Task<ActionResult<List<Author>>> Get()
        {
            throw new NotImplementedException();
            logger.LogInformation("This get new authors");
            service.WorkingPointer();
            return await context.Authors.Include(x => x.Books).ToListAsync();
        }

        [HttpGet("firts")] //Root: api/autores/firts
        public async Task<ActionResult<Author>> FirstAuthor()
        {
            return await context.Authors.Include(x => x.Books).FirstOrDefaultAsync();
        }

        [HttpGet("{id:int}/{param2=persona}")]
        public async Task<ActionResult<Author>> Get(int id, string param2)
        {
            var author = await context.Authors.Include(x => x.Books).FirstOrDefaultAsync(x => x.Id == id);

            if (author == null)
            {
                return NotFound();
            }

            return author;
        }


        [HttpGet("{name}")]
        public async Task<ActionResult<Author>> Get(string name)
        {
            var author = await context.Authors.Include(x => x.Books).FirstOrDefaultAsync(x => x.Name.Contains(name));
            if (author == null)
            {
                return NotFound();
            }
            return author;

        }


        [HttpPost]
        public async Task<ActionResult> Post(Author author)
        {
            context.Add(author);
            await context.SaveChangesAsync();
            return Ok();

        }

        [HttpPut("{id:int}")] // api/autores/1
        public async Task<ActionResult> Put(Author autor, int id)
        {
            if (autor.Id != id)
            {
                return BadRequest("The id not found for url id.");
            }

            var exist = await context.Authors.AnyAsync(x => x.Id == id);

            if (!exist)
            {
                return NotFound();
            }

            context.Update(autor);
            await context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id:int}")] // api/autores/2

        public async Task<ActionResult> Delete(int id)
        {

            var exist = await context.Authors.AnyAsync(x => x.Id == id);

            if (!exist)
            {
                return NotFound();
            }

            context.Remove(new Author() { Id = id });
            await context.SaveChangesAsync();
            return Ok();

        }

    }
}
